package jp.alhinc.uno_mari.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class CalculateSales {

	// ファイルのディレクトリ
	static String dir;
	// 支店定義マップ
	static HashMap<String, String> shopsInfoMap = new HashMap<String, String>();
	// 支店別集計マップ
	static HashMap<String, Integer> shopsAmountMap = new HashMap<String, Integer>();

	public static void main(String[] args) {

		// コマンドライン引数を取得する
		dir = args[0];
		System.out.println("以下のディレクトリ配下のファイルを参照します。： " + dir);

		// 支店定義の取得
		boolean isGetList = makeShopList();

		// 支店定義の取得に失敗したら終了
		if (!isGetList) {
			System.out.println("支店定義の取得に失敗しました。");
			System.exit(0);

		} else {
			System.out.println("支店定義の取得に成功しました。");
		}

		// 売上ファイルの集計
		boolean isClac = SaleClac();

		// 売上ファイルの集計に失敗したら終了
		if (!isClac) {
			System.out.println("売上ファイルの集計に失敗しました。");
			System.exit(0);

		} else {
			System.out.println("売上ファイルの集計に成功しました。");
		}

		// 集計結果の出力
		boolean isOutput = sumEvalOutput();

		// 売上ファイルの集計に失敗したら終了
		if (!isOutput) {
			System.out.println("集計結果の出力に失敗しました。");
			System.exit(0);

		} else {
			System.out.println("集計結果の出力に成功しました。");
		}

	}

	// 支店定義の取得
	private static boolean makeShopList() {
		FileReader filereader = null;
		BufferedReader bufferdReader = null;

		try {
			// 支店定義ファイルの取得
			File file = new File(dir + "\\branch.lst");

			// 支店定義ファイルがなかったら終了
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません。");
				return false;// 取得失敗
			}

			// FileReader、BufferedReader
			filereader = new FileReader(file);
			bufferdReader = new BufferedReader(filereader);

			String shopCode;// 支店コード
			String shopName;// 支店名
			String line;// ファイルから読み込んだ1行

			while ((line = bufferdReader.readLine()) != null) {

				// 支店コードを取得する
				shopCode = line.substring(0, 3);

				// 支店名を取得する
				shopName = line.substring(4);

				// 支店情報マップ
				shopsInfoMap.put(shopCode, shopName);

				// 支店別集計マップ
				shopsAmountMap.put(shopCode, 0);

			}
			return true;// 取得成功

		} catch (FileNotFoundException e) {
			System.out.println(e);
			return false;// 取得失敗

		} catch (IOException e) {
			System.out.println(e);
			return false;// 取得失敗

		} finally {
			try {
				// FileReader、BufferedReaderを閉じる
				filereader.close();
				bufferdReader.close();

			} catch (IOException e) {
				System.out.println(e);

			}
		}

	}

	// 売上ファイルの集計
	private static boolean SaleClac() {

		FileReader filereader = null;
		BufferedReader bufferdReader = null;

		try {

			// 売上ファイル存在フラグ
			boolean isExist = true;
			// 売上ファイルの連番
			int fileNum = 1;
			// 前ゼロ補完用の文字列
			String frontZero = "0000000";

			while (isExist) {

				// 前ゼロ補完 ＆ 8桁に変換
				String mergeText = frontZero + fileNum;
				String fileName = mergeText.substring(mergeText.length() - 8);

				// 指定のディレクトリ ＋ ファイル名
				fileName = dir + "\\" + fileName + ".rcd";

				try {
					// 売上ファイルの取得
					File file = new File(fileName);

					// FileReader、BufferedReader
					filereader = new FileReader(file);
					bufferdReader = new BufferedReader(filereader);

					// 売上ファイルから支店コードを取得
					String saleShopCode = bufferdReader.readLine();

					// 売上ファイルから売上金額を取得
					int SalesAmount = Integer.parseInt(bufferdReader.readLine());

					// 支店別集計マップから合計金額を取得して、売上金額を加算する
					int SalesAmountSum = shopsAmountMap.get(saleShopCode) + SalesAmount;

					// 支店別集計マップに代入する
					shopsAmountMap.put(saleShopCode, SalesAmountSum);

				} catch (FileNotFoundException e) {
					System.out.println(e);
					isExist = false;// 取得失敗

				} catch (IOException e) {
					System.out.println(e);
					return false;

				} catch (NumberFormatException e) {
					System.out.println(e);
					return false;

				}

				fileNum++;
			}

			return true;// 支店別集計リストの成功

		} finally {

			try {
				// FileReader、BufferedReaderを閉じる
				filereader.close();
				bufferdReader.close();

			} catch (IOException e) {
				System.out.println(e);

			}

		}
	}

	// 集計結果の出力
	private static boolean sumEvalOutput() {

		// 支店定義のキーを取得する
		Set<String> keys = shopsInfoMap.keySet();

		// 集計結果ファイル名
		File file = new File(dir + "\\branch.out");

		// BufferedWriter
		BufferedWriter bufferedWriter = null;

		try {

			// ファイルが存在しない場合は、ファイルを作成
			if (!file.exists()) {
				file.createNewFile();
			}

			bufferedWriter = new BufferedWriter(new FileWriter(file));

			for (int i = 0; i < keys.size(); i++) {

				if (i != 0) {
					// 改行
					bufferedWriter.newLine();
				}

				// キー
				String key = keys.toArray(new String[0])[i];
				// 支店名
				String shopName = shopsInfoMap.get(key);
				// 売上金額
				int saleAmount = shopsAmountMap.get(key);
				// ファイルの書き込み
				bufferedWriter.write(key + "," + shopName + "," + saleAmount);
				System.out.println(key + "," + shopName + "," + saleAmount);
			}
			bufferedWriter.close();

		} catch (IOException e) {
			System.out.println(e);

		}
		return true;
	}
}
