package jp.alhinc.uno_mari.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CalculateSales2 {

	public static void main(String[] args) {

		// 非staticなメソッドにアクセスするためのインスタンス
		CalculateSales2 calcSaleIns = new CalculateSales2();

		//コマンドライン引数の要素が1つじゃない場合はエラー
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		// コマンドライン引数を取得する
		String dir = args[0];

		// 支店定義マップ
		HashMap<String, String> branchInfoMap = new HashMap<String, String>();

		// 商品定義マップ
		HashMap<String, String> commodityInfoMap = new HashMap<String, String>();

		// 支店別集計マップ
		HashMap<String, Long> branchAmountMap = new HashMap<String, Long>();

		// 商品別集計マップ
		HashMap<String, Long> commodityAmountMap = new HashMap<String, Long>();

		// ファイルセパレーター
		String separator = File.separator;

		// 支店定義の取得
		String getBranchErrorMsg = calcSaleIns.makeDefinitionList(dir + separator + "branch.lst", branchInfoMap, branchAmountMap, "^[0-9]{3}$", "支店");

		// 支店定義の取得に失敗したら終了
		if (getBranchErrorMsg != null) {
			System.out.println(getBranchErrorMsg);
			return;
		}

		// 商品定義の取得
		String getCommodityErrorMsg = calcSaleIns.makeDefinitionList(dir + separator + "commodity.lst", commodityInfoMap, commodityAmountMap,
				"^[0-9a-zA-Z]{8}$", "商品");

		// 商品定義の取得に失敗したら終了
		if (getCommodityErrorMsg != null) {
			System.out.println(getCommodityErrorMsg);
			return;
		}

		// 売上ファイルの集計
		String clacErrorMsg = calcSaleIns.saleClac(dir, branchInfoMap, commodityInfoMap, branchAmountMap, commodityAmountMap);

		// 売上ファイルの集計に失敗したら終了
		if (clacErrorMsg != null) {
			System.out.println(clacErrorMsg);
			return;
		}

		// 支店別集計結果の出力
		String branchOutputErrorMsg = calcSaleIns.sumEvalOutput(dir + separator + "branch.out", branchInfoMap, branchAmountMap);

		// 売上ファイルの集計に失敗したら終了
		if (branchOutputErrorMsg != null) {
			System.out.println(branchOutputErrorMsg);
			return;
		}

		// 商品別集計結果の出力
		String commodityOutputErrorMsg = calcSaleIns.sumEvalOutput(dir + separator + "commodity.out", commodityInfoMap, commodityAmountMap);

		// 売上ファイルの集計に失敗したら終了
		if (commodityOutputErrorMsg != null) {
			System.out.println(commodityOutputErrorMsg);
			return;
		}

		return;
	}

	/* 定義ファイルの取得
	 *
	 * 引数
	 *  String filePath  定義ファイルのパス
	 *  HashMap<String, String> infoMap  定義マップ
	 *  HashMap<String, Long> amountMap  集計マップ
	 *  String regex  正規表現
	 *  String target  支店 or 商品
	*/
	private String makeDefinitionList(String filePath, HashMap<String, String> infoMap,
			HashMap<String, Long> amountMap, String regex, String target) {
		FileReader fileReader = null;
		BufferedReader bufferdReader = null;

		try {
			// 定義ファイルの取得
			File file = new File(filePath);

			// 定義ファイルがなかったら終了
			if (!file.exists()) {
				// 取得失敗
				return target + "定義ファイルが存在しません";
			}

			// FileReader、BufferedReader
			fileReader = new FileReader(file);
			bufferdReader = new BufferedReader(fileReader);

			// ファイルから読み込んだ1行
			String line;

			while ((line = bufferdReader.readLine()) != null) {

				String[] splitLine = line.split(",", 0);

				// 1行の要素数が2つ以外の場合はエラー
				if (splitLine.length != 2) {
					return target + "定義ファイルのフォーマットが不正です";
				}

				// 支店コードまたは商品コードを取得する
				String code = splitLine[0];

				// 支店名または商品名を取得する
				String name = splitLine[1];

				//正規表現チェック
				if (!code.matches(regex)) {
					return target + "定義ファイルのフォーマットが不正です";
				}

				// 支店情報マップまたは商品情報マップに代入する
				infoMap.put(code, name);

				// 支店別集計マップまたは商品別集計マップに代入する
				amountMap.put(code, 0L);

			}
			return null;

		} catch (FileNotFoundException e) {
			return "予期せぬエラーが発生しました";

		} catch (IOException e) {
			return "予期せぬエラーが発生しました";

		} finally {
			try {
				if (fileReader != null) {
					// FileReaderを閉じる
					fileReader.close();
				}
				if (bufferdReader != null) {
					// BufferedReaderを閉じる
					bufferdReader.close();
				}
			} catch (IOException e) {
				// 取得失敗
				return "予期せぬエラーが発生しました";

			}
		}

	}

	/* 売上ファイルの集計
	 *
	 * 引数
	 *  String dir  売上ファイルのディレクトリ
	 *  HashMap<String, String> branchInfoMap  支店定義マップ
	 *  HashMap<String, String> commodityInfoMap  商品定義マップ
	 *  HashMap<String, Long> branchAmountMap  支店別集計マップ
	 *  HashMap<String, Long> commodityAmountMap  商品別集計マップ
	*/
	private String saleClac(String dir, HashMap<String, String> branchInfoMap, HashMap<String, String> commodityInfoMap,
			HashMap<String, Long> branchAmountMap, HashMap<String, Long> commodityAmountMap) {

		// 指定のディレクトリ配下のファイルをすべて取得
		File targetDir = new File(dir);
		File fileList[] = targetDir.listFiles();

		// 売上ファイル一覧
		ArrayList<File> saleFileList = new ArrayList<File>();

		for (int i = 0; i < fileList.length; i++) {

			// ファイル名
			String fileName = fileList[i].getName();

			//半角数字(8桁)、拡張子がrcdのファイルを格納する
			if (fileName.matches("^[0-9]{8}.rcd$") && fileList[i].isFile() == true) {
				saleFileList.add(fileList[i]);
			}
		}

		// 売上ファイルリストを昇順でソートする
		Collections.sort(saleFileList);

		// 今の連番
		int nowSerialNum = 0;
		// 前の連番
		int previousSerialNum = 0;

		for (int i = 0; i < saleFileList.size(); i++) {

			// 売上ファイルリストに格納されたファイル名を取得
			String fileName = saleFileList.get(i).getName();

			//拡張子までの位置を取得
			int point = fileName.lastIndexOf(".");

			//ファイル名から拡張子を除く
			fileName = fileName.substring(0, point);

			// ファイル名をint型に変換する
			try {
				nowSerialNum = Integer.parseInt(fileName);

			} // int型への変換が失敗した場合
			catch (NumberFormatException e) {
				return "予期せぬエラーが発生しました";

			}
			// 初回じゃなければ連番かどうかチェックする
			if (i != 0) {

				// 今の連番が、前の連番＋1じゃない場合
				if (nowSerialNum != previousSerialNum + 1) {
					return "売上ファイル名が連番になっていません";
				}
			}
			previousSerialNum = nowSerialNum;
		}

		FileReader fileReader = null;
		BufferedReader bufferdReader = null;

		for (int i = 0; i < saleFileList.size(); i++) {

			try {
				// 売上ファイルの取得
				File file = new File(saleFileList.get(i).getPath());

				// FileReader、BufferedReader
				fileReader = new FileReader(file);
				bufferdReader = new BufferedReader(fileReader);

				// 売上ファイルの文字列(行単位)を保持するためのリスト
				ArrayList<String> lineDataList = new ArrayList<String>();

				// ファイルから読み込んだ1行
				String line;

				while ((line = bufferdReader.readLine()) != null) {
					//リストに1行ごと代入
					lineDataList.add(line);
				}

				// 規定の行数
				int regularLineSize = 3;

				// 行数が規定の行数じゃない場合はエラー
				if (lineDataList.size() != regularLineSize) {
					return file.getName() + "のフォーマットが不正です";
				}

				// 売上ファイルから支店コードを取得
				String branchCode = lineDataList.get(0);

				// 支店コードが支店定義マップに存在するか確認する
				if (!branchInfoMap.containsKey(branchCode)) {

					// 支店コードが存在しなければエラーにする
					return file.getName() + "の支店コードが不正です";
				}

				// 売上ファイルから商品コードを取得
				String commodityCode = lineDataList.get(1);

				// 商品コードが商品定義マップに存在するか確認する
				if (!commodityInfoMap.containsKey(commodityCode)) {
					// 商品コードが存在しなければエラーにする
					return file.getName() + "の商品コードが不正です";
				}

				// 売上ファイルから売上金額を取得
				long salesAmount = Long.parseLong(lineDataList.get(2));

				// 支店別集計マップから合計金額を取得して、売上金額を加算する
				long salesAmountSum = branchAmountMap.get(branchCode) + salesAmount;

				//最大金額桁数
				int maxSalesAmountLength = 10;

				// 合計金額(支店別)の桁数が、最大金額桁数を超えたらエラーにする
				if (String.valueOf(salesAmountSum).length() > maxSalesAmountLength) {
					return "合計金額が10桁を超えました";
				}

				// 支店別集計マップに代入する
				branchAmountMap.put(branchCode, salesAmountSum);

				// 商品別集計マップから合計金額を取得して、売上金額を加算する
				long commodityAmountSum = commodityAmountMap.get(commodityCode) + salesAmount;

				// 合計金額(商品別)の桁数が、最大金額桁数を超えたらエラーにする
				if (String.valueOf(commodityAmountSum).length() > maxSalesAmountLength) {
					return "合計金額が10桁を超えました";
				}

				// 商品別集計マップに代入する
				commodityAmountMap.put(commodityCode, commodityAmountSum);

			} catch (FileNotFoundException e) {
				return "予期せぬエラーが発生しました";

			} catch (IOException e) {
				return "予期せぬエラーが発生しました";

			} catch (NumberFormatException e) {
				return "予期せぬエラーが発生しました";

			} finally {

				try {
					if (fileReader != null) {
						// FileReaderを閉じる
						fileReader.close();
					}
					if (bufferdReader != null) {
						// BufferedReaderを閉じる
						bufferdReader.close();
					}
				} catch (IOException e) {
					return "予期せぬエラーが発生しました";

				}

			}
		}
		return null;// 支店別集計リストの成功
	}

	/* 売上ファイルの集計
	 *
	 * 引数
	 *  String dir  集計結果ファイルのパス
	 *  HashMap<String, Long> branchAmountMap  支店別集計マップ
	 *  HashMap<String, Long> commodityAmountMap  商品別集計マップ
	*/
	private String sumEvalOutput(String filePath, HashMap<String, String> infoMap, HashMap<String, Long> amountMap) {

		// 集計結果ファイル
		File file = new File(filePath);

		// BufferedWriter
		BufferedWriter bufferedWriter = null;

		// 売上金額の降順で並び替え
		List<Map.Entry<String, Long>> entries = new ArrayList<Map.Entry<String, Long>>(amountMap.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<String, Long>>() {
			public int compare(Entry<String, Long> entry1, Entry<String, Long> entry2) {
				return ((Long) entry2.getValue()).compareTo((Long) entry1.getValue());
			}
		});

		try {

			bufferedWriter = new BufferedWriter(new FileWriter(file));

			// ファイルが存在しない場合は、ファイルを作成
			if (!file.exists()) {
				file.createNewFile();
			}

			// 売上金額の降順でキーを取得する
			for (Entry<String, Long> s : entries) {

				// 支店コードまたは商品コード
				String key = s.getKey();

				// 支店名または商品名
				String name = infoMap.get(key);

				// 売上金額
				long saleAmount = amountMap.get(key);

				// ファイルの書き込み
				bufferedWriter.write(key + "," + name + "," + saleAmount);

				// 改行
				bufferedWriter.newLine();
			}

		} catch (IOException e) {
			return "予期せぬエラーが発生しました";

		} catch (Exception e) {
			return "予期せぬエラーが発生しました";
		}
		if (bufferedWriter != null) {
			try {
				// BufferedReaderを閉じる
				bufferedWriter.close();
			} catch (IOException e) {
				System.out.println(e);
			}
		}
		return null;

	}
}
